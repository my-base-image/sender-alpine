FROM alpine:3.8

RUN apk update && apk upgrade \
    && apk add --no-cache openssh rsync curl \
    && mkdir ~/.ssh \
    && chmod 700 ~/.ssh

